package mediumapp;

import java.util.List;

public class PostGetterAndSetter {
    private String publishStatus;
    private String contentFormat;
    private String content;
    private List<String> tags;

    public String getPublishStatus(){
        return this.publishStatus;
    }
    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getContentFormat(){
        return this.contentFormat;
    }
    public void setContentFormat(String contentFormat) {
        this.contentFormat = contentFormat;
    }

    public String getContent(){
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public void setCourses(List<String> courses) {
        this.tags = tags;
    }
}
