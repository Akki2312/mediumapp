package mediumapp.serenity;

import mediumapp.PostGetterAndSetter;
import mediumapp.utils.PropertiesReader;
import mediumapp.utils.ReusableSpecifications;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import java.io.IOException;
import java.util.HashMap;

public class MediumSerenitySteps {

    PropertiesReader reader = new PropertiesReader("properties-from-pom.properties");
    String gitlabToken = reader.getProperty("token");

    public MediumSerenitySteps() throws IOException {}

    String localToken = System.getenv("MEDIUM_TOKEN");
    String token = localToken== null ? gitlabToken :localToken;

    public boolean checkToken () {
        if (token != null) {
            return true;
        } else return false;
    }

    public Response getUserDetails(String url){

        Response response = SerenityRest.given()
                .header("Authorization","Bearer " + this.token)
                .get(url);

        return response;
    }

    public Response createPost(String contentFormat,String content, String publishStatus ,String endpoint ) {

        HashMap<String,Object> userExpectedData = this.getUserDetails("/me").then().extract().path("data");
        String userId= (String) userExpectedData.get("id");

        PostGetterAndSetter post = new PostGetterAndSetter();
        post.setContentFormat(contentFormat);
        post.setContent(content);
        post.setPublishStatus(publishStatus);

        Response createPostResponse = SerenityRest.given()
                .header("Authorization","Bearer " + this.token)
                .spec(ReusableSpecifications.getGenericRequestSpec())
                 .when()
                 .body(post)
                 .post("users/" + userId + endpoint);

        return createPostResponse;
    }

    public Response getPublicationList(String publicationUrl){
        HashMap<String,Object> userExpectedData = this.getUserDetails("/me").then().extract().path("data");
        String userId= (String) userExpectedData.get("id");
        Response response = SerenityRest.given()
                .header("Authorization","Bearer " + this.token)
                .get("users/" + userId + publicationUrl);

        return response;
    }

}
