package mediumapp.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class TestUtils {
    /**
     * Returns a random string
     * @return
     */
    public static String getRandomString(){
        String generatedString = RandomStringUtils.randomAlphabetic(10);
        return generatedString;
    };
}
