package mediumapp.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import mediumapp.serenity.MediumSerenitySteps;
import cucumber.api.java.en.When;
import org.jruby.RubyProcess;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class UserDetailsSteps {

    MediumSerenitySteps medium = new MediumSerenitySteps();

    public UserDetailsSteps() throws IOException {
    }


    @Given("^User token should be present$")
    public void user_token_should_be_present() {

        Boolean correct = medium.checkToken();
        assertEquals(true, correct);

    }
    @When("^User send a GET request to endpoint \"([^\"]*)\"$")
    public void user_send_a_get_request_to_endpoint(String endpoint) {
        medium.getUserDetails(endpoint).then().statusCode(200);
    }


    @Then("^User should get valid user details$")
    public void user_should_get_valid_user_details(List<Map<String, String>> userDetails)  {

        HashMap<String,Object> userActualDetails= medium.getUserDetails("/me").then().statusCode(200).extract().path("data");
        Map<String, String> userExpectedDetails = userDetails.get(0);

        String  userExpectedName = userExpectedDetails.get("name");
        String  userExpectedUserName = userExpectedDetails.get("username");
        String  userExpectedUrl=  userExpectedDetails.get("myUrl");

        String userActualName = (String) userActualDetails.get("name");
        String userActualUserName = (String) userActualDetails.get("username");
        String userActualUrl= (String) userActualDetails.get("url");

        assertEquals(userExpectedName, userActualName);
        assertEquals(userExpectedUserName, userActualUserName);
        assertEquals(userExpectedUrl, userActualUrl);

    }





}
