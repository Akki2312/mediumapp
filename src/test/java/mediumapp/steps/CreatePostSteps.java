package mediumapp.steps;

import cucumber.api.java.en.Given;
import mediumapp.serenity.MediumSerenitySteps;
import mediumapp.utils.TestUtils;
import cucumber.api.java.en.When;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;

public class CreatePostSteps {

    MediumSerenitySteps medium = new MediumSerenitySteps();

    public CreatePostSteps() throws IOException {
    }


    @Given("^User token should be present for post request with invalid publish status$")
    public void user_token_should_be_present_for_post_request_with_invalid_publish_status() {
        Boolean correct = medium.checkToken();
        assertEquals(true, correct);

    }

    @When("^User send a POST request with invalid publish status to endpoint \"([^\"]*)\" then it gives \"([^\"]*)\"$")
    public void user_send_a_post_request_with_invalid_publish_status_to_endpoint_then_it_gives_status_code(String endpoint, int expectedStatus, List<Map<String, String>> request) {

        Map<String, String> requestBody = request.get(0);

        String publishStatus = requestBody.get("publishStatus");
        String contentFormat = requestBody.get("contentFormat");
        String content = "<h1>" + requestBody.get("contentFormat") + " the title is gibberish " + TestUtils.getRandomString() + "</h1><p> Please pronounce this " + TestUtils.getRandomString() + "</p>";
        int actualStatusCode = medium.createPost(contentFormat, content, publishStatus,endpoint).getStatusCode();

        assertEquals(expectedStatus, actualStatusCode);
    }

    @Given("^User token should be present for post request with invalid content$")
    public void user_token_should_be_present_for_post_request_with_invalid_content() {
        Boolean correct = medium.checkToken();
        assertEquals(true, correct);
    }

    @When("^User send a POST request with invalid content type status to endpoint \"([^\"]*)\" then it gives \"([^\"]*)\"$")
    public void user_send_a_post_request_with_invalid_content_type_status_to_endpoint_something_then_it_gives_something(String endpoint, int expectedStatus, List<Map<String, String>> request) {
        Map<String, String> requestBody = request.get(0);

        String publishStatus = requestBody.get("publishStatus");
        String contentFormat = requestBody.get("contentFormat");
        String content = "<h1>" + requestBody.get("contentFormat") + " the title is gibberish " + TestUtils.getRandomString() + "</h1><p> Please pronounce this " + TestUtils.getRandomString() + "</p>";


        int actualStatusCode = medium.createPost(contentFormat, content, publishStatus, endpoint).getStatusCode();

        assertEquals(expectedStatus, actualStatusCode);
    }
    @Given("^User token should be present for post request with valid body$")
    public void user_token_should_be_present_for_post_request_with_valid_body()  {
        Boolean correct = medium.checkToken();
        assertEquals(true, correct);


    }

    @When("^User send a POST request with valid body to endpoint \"([^\"]*)\" then it gives \"([^\"]*)\"$")
    public void user_send_a_post_request_with_valid_body_to_endpoint_something_then_it_gives_something(String endpoint, int expectedStatus, List<Map<String, String>> request) {
        Map<String, String> requestBody = request.get(0);

        String publishStatus = requestBody.get("publishStatus");
        String contentFormat = requestBody.get("contentFormat");
        String content = "<h1>" + requestBody.get("contentFormat") + " the title is gibberish " + TestUtils.getRandomString() + "</h1><p> Please pronounce this " + TestUtils.getRandomString() + "</p>";


        int actualStatusCode = medium.createPost(contentFormat, content, publishStatus, endpoint).getStatusCode();
        assertEquals(expectedStatus, actualStatusCode);
    }


}
