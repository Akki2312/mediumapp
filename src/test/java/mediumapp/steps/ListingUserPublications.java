package mediumapp.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import mediumapp.serenity.MediumSerenitySteps;
import cucumber.api.java.en.When;
import org.jruby.RubyProcess;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ListingUserPublications {
    MediumSerenitySteps medium = new MediumSerenitySteps();

    public ListingUserPublications() throws IOException {
    }


    @Given("^User token to fetch list of publication should be present$")
    public void user_token_to_fetch_list_of_publication_should_be_present() {

        Boolean correct = medium.checkToken();
        assertEquals(true, correct);
    }

    @When("^User send a GET request to the \"([^\"]*)\" endpoint then it should return \"([^\"]*)\"$")
    public void user_send_a_get_request_to_the_publication_endpoint(String publicationUrl, int expectedStatus) {
        int actualStatusCode=  medium.getPublicationList(publicationUrl).getStatusCode();
        assertEquals(expectedStatus, actualStatusCode);
    }
    @Then("^User should have publications details for \"([^\"]*)\"$")
    public void user_should_have_publications(String publicationUrl, List<Map<String, String>> publicationDetails )  {
         List publicationActualDetail= medium.getPublicationList(publicationUrl).then().extract().path("data");

         HashMap<String,Object> publicationActualDetails = (HashMap<String, Object>) publicationActualDetail.get(0);
         Map<String, String> publicationExpectedDetails = publicationDetails.get(0);

         String  publicationExpectedId = publicationExpectedDetails.get("id");
         String  publicationExpectedName = publicationExpectedDetails.get("name");



        assertEquals(publicationExpectedId, publicationExpectedDetails.get("id"));
        assertEquals(publicationExpectedName,publicationActualDetails.get("name"));
    }

}