package mediumapp;

import mediumapp.testbase.TestBase;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = "pretty",features = "src/test/resources/feature/")
public class MediumRunner extends TestBase {


}
