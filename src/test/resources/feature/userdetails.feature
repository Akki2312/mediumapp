 Feature: Getting the authenticated user’s details
  Returns details of the user who has granted permission to the application.

  Scenario: Authenticated user should retrieve correct details

    Given User token should be present
    When User send a GET request to endpoint "/me"
    Then User should get valid user details
       | username     |             myUrl               | name          |
       | gupta.akki23 | https://medium.com/@gupta.akki23| Akshaya Gupta |