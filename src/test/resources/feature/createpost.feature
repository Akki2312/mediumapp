Feature: Creating a post
  Creates a post on the authenticated user’s profile.

  Scenario: Authenticated user should not be able to create a post with invalid publish status
    Given User token should be present for post request with invalid publish status
    When User send a POST request with invalid publish status to endpoint "/posts" then it gives "400"
      | contentFormat|  content | publishStatus |
      | html         |  My blog | publi         |

  Scenario: Authenticated user should not be able to create a post with invalid content type
    Given User token should be present for post request with invalid content
    When User send a POST request with invalid content type status to endpoint "/posts" then it gives "400"
      | contentFormat|  content | publishStatus |
      | htm          |  My blog | public        |

  Scenario: Authenticated use be able to create a post with valid body
    Given User token should be present for post request with valid body
    When User send a POST request with valid body to endpoint "/posts" then it gives "201"
      | contentFormat|  content | publishStatus |
      | html         |  My blog | public      |





