Feature: Listing the user’s publications

  Returns a full list of publications that the user is related to in some way.
  This includes all publications the user is subscribed to, writes to, or edits.

  Scenario: Authenticated user should be able to fetch list of publication related to user
    Given User token to fetch list of publication should be present
    When User send a GET request to the "/publications" endpoint then it should return "200"
    Then User should have publications details for "/publications"
      |     name           |   id         |
      | Picnic Engineering |  12f9dce2953c|

