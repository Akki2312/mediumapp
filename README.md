# Serenity-BDD-Medium-RestAPI

This is the learning and developing project for rest api test automation using serenity bdd

## Requirements
- git
- Java 1.8
- maven

## Instructions for OsX to run local

- Clone the repo and move in to repo folder 
  `cd medium app`
- Create your integrated token on medium app and set in env file on your machine 
  
    `export MEDIUM_TOKEN="####"`
- Update your user and publish feature file with your data
   `medium-app/src/test/resource/feature`
- Run the following command to execute the tests
  `mvn clean verify serenity:aggregate`
- The report will be available in the folder `target/site/serenity/index.html`. \
  Open in Chrome browser

## Instructions to  run on Gitlab
- Navigate to CI/CD and click on "Run Pipeline" 
- Result url `https://akki2312.gitlab.io/mediumapp/`

## Instructions to create new test for new feature 
-  Create a feature file
-  Create step definitions
-  Create a function required to perform an action
-  Call that function in your step definition file
-  Create assertion in your step definition file 

## End Point documentation  used in framework 
- Getting the authenticated user’s details

  `GET https://api.medium.com/v1/me`

- Creates a post on the authenticated user’s profile.

  `POST https://api.medium.com/v1/users/{{authorId}}/posts`

- Returns a full list of publications that the user is related to in some way

  `GET https://api.medium.com/v1/users/{{userId}}/publications`

- For details please go through following link.
  
  [Medium API]

[Medium API]: https://github.com/Medium/medium-api-docs

### The project directory structure
The project has build scripts for Maven  and follows the standard directory structure used in Serenity projects:
```Gherkin
src
  + main
    +java
      +mediumApp                          Gettersetter
  + test
    + java    
      +mediumapp
       +serenity                          Functions and Steps
       +steps                             StepDefinations
       +testbase                          Basesetup
       +utils                             Utilities 
       +MediumRunner                      Runner
    + resources
      + features                          Feature files 
        

